package aspects;

import java.rmi.RemoteException;

import server.LogMessage;
import server.Server;
import server.AuctionLogger;

public aspect LoggingAspect {

	pointcut bidOnItemSrv(Server c, String bidderName, String itemName, double bid):
		target(c) && args(bidderName, itemName, bid) && execution(void *.bidOnItem(String, String, double));

	pointcut placeItemForBidSrv(Server c, String ownerName, String itemName, String itemDesc, double startBid,
			int auctionTime):
		target(c) && args(ownerName, itemName, itemDesc, startBid, auctionTime) && execution(void *.placeItemForBid(String, String, String, double, int));

	before(Server c, String bidderName, String itemName, double bid) throws RemoteException:
		bidOnItemSrv(c, bidderName, itemName, bid) {
		AuctionLogger.getInstance()
				.log(new LogMessage("Placing bid on item: " + itemName + ", by:" + bidderName, Server.class));
	}

	after(Server c, String bidderName, String itemName, double bid) throws RemoteException:
		bidOnItemSrv(c, bidderName, itemName, bid) {
		AuctionLogger.getInstance().log(new LogMessage("Item not found! " + itemName.toString(), Server.class));
	}

	before(Server c, String ownerName, String itemName, String itemDesc, double startBid, int auctionTime)
			throws RemoteException:
		placeItemForBidSrv(c, ownerName, itemName, itemDesc, startBid, auctionTime) {
		AuctionLogger.getInstance()
				.log(new LogMessage("Placing item for bid: " + itemName + ", it desc.:" + itemDesc, Server.class));
	}

	after(Server c, String ownerName, String itemName, String itemDesc, double startBid, int auctionTime)
			throws RemoteException:
		placeItemForBidSrv(c, ownerName, itemName, itemDesc, startBid, auctionTime) {
		AuctionLogger.getInstance().log(new LogMessage("Item placed for bid.", Server.class));
	}
}
