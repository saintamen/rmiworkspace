package server;


import java.io.*;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by amen on 12/5/17.
 */
public class Database {
    private Map<String, Item> items = new HashMap<>();
    private PrintWriter writer, writer_bids;

    public Database() {
        // writer open
        try {
            this.writer = new PrintWriter(new FileWriter("database.db", true));
        } catch (IOException e) {
            System.err.println("DB Error: " + e.getMessage());
            e.printStackTrace();
        }

        // writer bids open
        try {
            this.writer_bids = new PrintWriter(new FileWriter("database_bids.db", true));
        } catch (IOException e) {
            System.err.println("DB Error: " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    public void load() {
    	// read everything
        try (BufferedReader reader = new BufferedReader(new FileReader("database.db"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] splits = line.split(";;");
                items.put(splits[0], new Item(splits[2], splits[0], splits[1], Double.parseDouble(splits[4]), Integer.parseInt(splits[3])));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveItemToDB(Item newItem) throws RemoteException {
        if (!items.containsKey(newItem.getName())) {
            writer.println(composeString(newItem));
            writer.flush();
        } else {
            throw new RemoteException("Object with that name already exists.");
        }
    }

    private String composeString(Item newItem) {
        return newItem.getName() + ";;" + newItem.getDescription() + ";;" + newItem.getOwnerName() + ";;" + newItem.getRemainingTime() + ";;" + newItem.getCurrentBid();
    }

    public void saveBidToDB(String bidderName, String itemName, double bid) {
        writer_bids.println(itemName + ";;;" + bidderName + ";;;" + bid);
        writer_bids.flush();
    }
}
