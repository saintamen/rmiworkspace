package server;

// Factory GoF
public class ServerFactory {
	public Server createServer() {
		return Server.instance();
	}
}