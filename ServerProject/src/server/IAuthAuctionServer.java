package server;
import java.rmi.RemoteException;

/**
 * Created by amen on 12/5/17.
 */
public interface IAuthAuctionServer extends IAuctionServer{
    public void login(IAuthAuctionListener observer) throws RemoteException;
}
