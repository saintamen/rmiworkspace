package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class Server implements IAuthAuctionServer {
	
	private static Server uniqueInstance = null;

	private Map<String, String> credentials = CredentialsLoader.loadCredentials();
	
	// Singleton GoF
	public static synchronized Server instance() {
		if (uniqueInstance == null) {
			try {
				uniqueInstance = new Server();
			}
			catch (Exception e) {
				System.err.println("Failed to create instance of server!");
				e.printStackTrace();
			}
		}
		
		return uniqueInstance;
	}

	protected List<Item> itemList = new ArrayList<Item>();
	
	//IAuctionServer implementation
	
	//add new item to be bidded!
	//Exception: thrown, when itemName is already in base.
	@Override
	public void placeItemForBid(String ownerName, String itemName, String itemDesc, double startBid, int auctionTime) throws RemoteException {
		for(Item item : itemList) {
			if (item.getName().equals(itemName)) {
				throw new RemoteException("It's me with name " + itemName + " is already on auction!");
			}
		}
		
		//if name is unique, let's continue
		Item newItem = new Item(ownerName, itemName, itemDesc, startBid, auctionTime);
		itemList.add(newItem);
		
		//Output for debugging
		System.out.println("placeItemForBid");
	}
	
	//do a bid on Item
	@Override
	public void bidOnItem(String bidderName, String itemName, double bid) throws RemoteException {
		for(Item item : itemList) {
			if (item.getName().equals(itemName)) {
				if (item.setCurrentBid(bid, bidderName)) {
					System.out.printf("%s is bid for %f by %s\n", itemName, bid, bidderName);
				}
				else {
					throw new RemoteException("Unable to bid! Auction is over or price to low!");
				}
				return;
			}
		}
		
		throw new RemoteException("Item not found! " + itemName.toString());
	}
	
	//returns auctions to bid
	@Override
	public Item[] getItems() throws RemoteException {
		if (itemList.size() == 0) {
			return null;
		}
		
		return itemList.toArray(new Item[itemList.size()]);
	}
	
	//attachs client to item notification
	@Override
	public void registerListener(IAuctionListener auctionListener, String itemName) throws RemoteException {
		for(Item item : itemList) {
			if (item.getName().equals(itemName)) {
				item.addListener(auctionListener);
				return;
			}
		}
		
		throw new RemoteException("Item not found! " + itemName.toString());
	}
	
	protected Server() throws Exception {
		String name = "AuctionServ";
		IAuthAuctionServer engine = this;
		IAuthAuctionServer stub = (IAuthAuctionServer) UnicastRemoteObject.exportObject(engine, 0);
        Registry registry = LocateRegistry.getRegistry();
        registry.rebind(name, stub);
        System.out.println("AuctionServ bound");
	}

    public static void main(String[] args) {
        if (System.getSecurityManager() == null) {
        	System.setSecurityManager(new SecurityManager());
        }
        
        try {
        	ServerFactory serverFactory = new ServerFactory();
        	Server server = serverFactory.createServer();
        	
        	server.placeItemForBid("Adam", "Krzeslo", "Nice description", 15.0, 300);
        	server.placeItemForBid("Ewa", "Sukienka", "Nice description2", 10.0, 300);
        	server.placeItemForBid("Tola", "Ksiazka3", "Nice description3", 5.0, 300);
            
            System.out.println("Auction Server is ready to go!");
            
        } catch (Exception e) {
            System.err.println("AuctionServ exception:");
            e.printStackTrace();
        }
    }

	@Override
	public void login(IAuthAuctionListener observer) throws RemoteException {
		if (!credentials.get(observer.getLogin()).equals(observer.getPass())) {
			throw new RemoteException("wrong password for " + observer.getLogin());
		}
	}
}
