import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class Server implements IAuthAuctionServer {

    private static ServerImpl uniqueInstance = null;

    // Singleton GoF
    public static synchronized ServerImpl instance() {
        if (uniqueInstance == null) {
            try {
                uniqueInstance = new ServerImpl();
            } catch (Exception e) {
                ServerLogger.getInstance().log(new LogMessage("Error creating server instance.", ServerImpl.class));
                System.err.println("Failed to create instance of server!");
                e.printStackTrace();
            }
        }

        return uniqueInstance;
    }

    protected List<Item> itemList = new ArrayList<Item>();
    private Map<String, String > credentials = CredentialsLoader.loadCredentials();
    private Database database = new Database();

    //IAuctionServer implementation

    //add new item to be bidded!
    //Exception: thrown, when itemName is already in base.
    @Override
    public void placeItemForBid(String ownerName, String itemName, String itemDesc, double startBid, int auctionTime) throws RemoteException {
        ServerLogger.getInstance().log(new LogMessage("Placing item for bid: " + itemName + ", it desc.:" + itemDesc, ServerImpl.class));
        for (Item item : itemList) {
            if (item.getName().equals(itemName)) {
                throw new RemoteException("It's me with name " + itemName + " is already on auction!");
            }
        }

        //if name is unique, let's continue
        Item newItem = new Item(ownerName, itemName, itemDesc, startBid, auctionTime);
        database.saveItemToDB(newItem);
        itemList.add(newItem);

        //Output for debugging
        System.out.println("placeItemForBid");
        ServerLogger.getInstance().log(new LogMessage("Item placed for bid.", ServerImpl.class));
    }

    //do a bid on Item
    @Override
    public void bidOnItem(String bidderName, String itemName, double bid) throws RemoteException {
        ServerLogger.getInstance().log(new LogMessage("Placing bid on item: " + itemName + ", by:" + bidderName, ServerImpl.class));
        for (Item item : itemList) {
            if (item.getName().equals(itemName)) {
                if (item.setCurrentBid(bid, bidderName)) {
                    System.out.printf("%s is bid for %f by %s\n", itemName, bid, bidderName);
                    database.saveBidToDB(bidderName, itemName, bid);

                    ServerLogger.getInstance().log(new LogMessage(itemName + " is bid for " + bid + " by " + bidderName, ServerImpl.class));
                } else {
                    ServerLogger.getInstance().log(new LogMessage("Unable to bid! Auction is over or price to low!", ServerImpl.class));
                    throw new RemoteException("Unable to bid! Auction is over or price to low!");
                }
                return;
            }
        }

        ServerLogger.getInstance().log(new LogMessage("Item not found! " + itemName.toString(), ServerImpl.class));
        throw new RemoteException("Item not found! " + itemName.toString());
    }

    //returns auctions to bid
    @Override
    public Item[] getItems() throws RemoteException {
        ServerLogger.getInstance().log(new LogMessage("Getting items list", ServerImpl.class));
        if (itemList.size() == 0) {
            return null;
        }

        return itemList.toArray(new Item[itemList.size()]);
    }

    //attachs client to item notification
    @Override
    public void registerListener(IAuctionListener auctionListener, String itemName) throws RemoteException {
        ServerLogger.getInstance().log(new LogMessage("Register listener", ServerImpl.class));
        for (Item item : itemList) {
            if (item.getName().equals(itemName)) {
                ServerLogger.getInstance().log(new LogMessage("Register listener - successful", ServerImpl.class));
                item.addListener(auctionListener);
                return;
            }
        }
        ServerLogger.getInstance().log(new LogMessage("Register listener - unsuccessful - item not found", ServerImpl.class));
        throw new RemoteException("Item not found! " + itemName.toString());
    }

    protected Server() throws Exception {
        String name = "AuctionServ";
        IAuctionServer engine = this;
        IAuctionServer stub = (IAuctionServer) UnicastRemoteObject.exportObject(engine, 0);
        Registry registry = LocateRegistry.getRegistry();
        registry.rebind(name, stub);
        System.out.println("AuctionServ bound");
        ServerLogger.getInstance().log(new LogMessage("Server bound", ServerImpl.class));
    }

    public static void main(String[] args) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        try {
            ServerFactory serverFactory = new ServerFactory();
            ServerImpl server = serverFactory.createServer();

            server.placeItemForBid("Adam", "Krzeslo", "Nice description", 15.0, 300);
            server.placeItemForBid("Ewa", "Sukienka", "Nice description2", 10.0, 300);
            server.placeItemForBid("Tola", "Ksiazka3", "Nice description3", 5.0, 300);

            System.out.println("Auction Server is ready to go!");

        } catch (Exception e) {
            System.err.println("AuctionServ exception:");
            e.printStackTrace();
        }
    }

    @Override
    public void login(IAuthAuctionListener observer) throws RemoteException {
        if (!credentials.get(observer.getLogin()).equals(observer.getPass())) {
            throw new RemoteException("wrong password for " + observer.getLogin());
        }
    }
}
