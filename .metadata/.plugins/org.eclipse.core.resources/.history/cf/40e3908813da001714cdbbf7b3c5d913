package aspects;

import java.rmi.RemoteException;

import server.AuctionLogger;
import server.Database;
import server.Item;
import server.LogMessage;
import server.Server;

public aspect DatabaseAspect {

	private Database database = new Database();
	private boolean itemsLoaded = false;

	pointcut getItemsSrv(Server c):
		target(c) && args() && execution(Item[] *.getItems());

	before(Server c) throws RemoteException:
		getItemsSrv(c) {
		if (!itemsLoaded) {
			database.load();
		}
	}
	
	pointcut bidOnItemSrv(Server c, String bidderName, String itemName, double bid):
		target(c) && args(bidderName, itemName, bid) && execution(void *.bidOnItem(String, String, double));

	pointcut placeItemForBidSrv(Server c, String ownerName, String itemName, String itemDesc, double startBid,
			int auctionTime):
		target(c) && args(ownerName, itemName, itemDesc, startBid, auctionTime) && execution(void *.placeItemForBid(String, String, String, double, int));

	after(Server c, String bidderName, String itemName, double bid) throws RemoteException:
		bidOnItemSrv(c, bidderName, itemName, bid) {
		AuctionLogger.getInstance().log(new LogMessage("Item not found! " + itemName.toString(), Server.class));
	}

	after(Server c, String ownerName, String itemName, String itemDesc, double startBid, int auctionTime)
			throws RemoteException:
		placeItemForBidSrv(c, ownerName, itemName, itemDesc, startBid, auctionTime) {
		AuctionLogger.getInstance().log(new LogMessage("Item placed for bid.", Server.class));
	}
}
