package server;
/**
 * Created by amen on 12/5/17.
 */
public class LogMessage {
    private String message;
    private Class where;

    public LogMessage(String message, Class where) {
        this.message = message;
        this.where = where;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Class getWhere() {
        return where;
    }

    public void setWhere(Class where) {
        this.where = where;
    }

    @Override
    public String toString() {
        return "LogMessage{" +
                "message='" + message + '\'' +
                ", where=" + where +
                '}';
    }
}
