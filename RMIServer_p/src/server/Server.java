package server;
import java.rmi.RemoteException;

public class Server {
	private final static Server instance = new Server();
	private ServerImpl server;
	
	public static ServerImpl getInstance() {
		return instance.getServer();
	}
	
	private Server() {
		// comment out right server
		try {
			server = ServerFactory.createLoggedServer();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			server = null;
			
			e.printStackTrace();
		}
	}
	
	public ServerImpl getServer() {
		return server;
	}
}
