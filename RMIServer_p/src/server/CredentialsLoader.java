package server;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CredentialsLoader {
    public static Map<String, String> loadCredentials() {
        Map<String, String> credentials = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("pwd.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] passEntry = line.split("=");

                credentials.put(passEntry[0], passEntry[1]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return credentials;
    }
}
