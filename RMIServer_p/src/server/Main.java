package server;

public class Main {
	public static void main(String[] args) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        try {
            ServerFactory serverFactory = new ServerFactory();
            ServerImpl server = Server.getInstance();

            server.placeItemForBid("Adam", "Krzeslo", "Nice description", 15.0, 300);
            server.placeItemForBid("Ewa", "Sukienka", "Nice description2", 10.0, 300);
            server.placeItemForBid("Tola", "Ksiazka3", "Nice description3", 5.0, 300);

            System.out.println("Auction Server is ready to go!");

        } catch (Exception e) {
            System.err.println("AuctionServ exception:");
            e.printStackTrace();
        }
    }
}
