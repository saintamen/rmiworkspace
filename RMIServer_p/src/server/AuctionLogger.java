package server;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by amen on 12/5/17.
 */
public class AuctionLogger {
    private static final AuctionLogger instance = new AuctionLogger();

    private PrintWriter logFileWriter;

    private AuctionLogger() {
        try {
            this.logFileWriter = new PrintWriter(new FileWriter(new File("out.log")));
        } catch (IOException e) {
            System.out.println("Logger unavailable: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void log(LogMessage message) {
        logFileWriter.println(System.currentTimeMillis() + " => " + message);
        logFileWriter.flush();
    }

    public static AuctionLogger getInstance() {
        return instance;
    }
}
