package server;
import java.rmi.RemoteException;

// Factory GoF
public class ServerFactory {
	
	public static ServerImpl createLoggedServer() throws RemoteException{
		return new LoggingServerImpl();
	}

	public static ServerImpl createBasicServer() throws RemoteException{
		// TODO Auto-generated method stub
		return new ServerImpl();
	}
}