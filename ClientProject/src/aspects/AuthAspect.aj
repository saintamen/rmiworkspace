package aspects;

import java.util.Scanner;

import client.Client;

public aspect AuthAspect {

	pointcut connectClient(Client c, String arg):
		target(c) && args(arg) && execution(void *.connect(String));

	after(Client c, String arg) throws Exception:
		connectClient(c, arg) {

		System.out.println("Authenticate: ");
		System.out.println("Login: ");
		c.setLogin(new Scanner(System.in).nextLine());
		System.out.println("Password: ");
		c.setPass(new Scanner(System.in).nextLine());
		
		try {
			c.getAuctionServer().login(c);
		} catch (Exception e) {
			System.exit(0);
			System.out.println("Login error!");
		}
	}
}
