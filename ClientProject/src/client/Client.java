package client;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.math.BigDecimal;
import server.*;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

public class Client extends UnicastRemoteObject implements IAuthAuctionListener {

	protected String name;
	protected String surname;
	protected String email;
	protected String address;
	protected String login;
	protected String pass;
	protected IAuthAuctionServer auctionServer;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}

	private static Client uniqueInstance = null;
	
	// Singleton GoF
	public static synchronized Client instance() {
		if (uniqueInstance == null) {
			try {
				uniqueInstance = new Client();
			}
			catch (RemoteException e) {
				System.out.println("Client class exception!");
				e.printStackTrace();
			}
		}
		
		return uniqueInstance;
	}
	
	//Strategy GoF
	protected Strategy biddingStrategy = null;
	
	public void setStrategy(Strategy strategy) {
		biddingStrategy = strategy;
		
		if (strategy != null) {
			System.out.println("Using strategy: " + strategy.toString());
		}
		else {
			System.out.println("Automatic bid is disabled.");
		}
	}
	
	//Invoked by the auction server for each IAuctionListener which has registered to be notified of changes in the bid status of the specified item.
	//Observer GoF
	public void update(Item item) throws RemoteException {
		if (biddingStrategy != null) {
			//perform a strategy
			biddingStrategy.updateWithStrategy(item, this, auctionServer);
		}
		else {
			System.out.println("Sir/Madam, you're notified about change, but you don't have a strategy!");
		}
	}
	
	protected Client() throws RemoteException {
		super();
	}
	
	public void connect(String arg) throws Exception {
		String name = "AuctionServ";
        Registry registry = LocateRegistry.getRegistry(arg);
        auctionServer = (IAuthAuctionServer) registry.lookup(name);
	}
	
    public static void main(String args[]) {
        if (System.getSecurityManager() == null) {
        	System.setSecurityManager(new SecurityManager());
        }
        try {
            Client client = Client.instance();
            client.connect(args[0]);
            client.run();
        } catch (Exception e) {
            System.err.println("Server exception:");
            e.printStackTrace();
        }
    }
    
    public void run() {
    	System.out.println("What's your name?");
    	name = new Scanner(System.in).nextLine();
    	
    	while(true) {
    		printMenu();
    		switch (getMenuPosition(0, 5)) {
    		case 0:
    			System.out.println("Bye!");
    			return;
    		case 1:
    			printPendingAuctions();
    			break;
    		case 2:
    			placeItemOnAuction();
    			break;
    		case 3:
    			bidItem();
    			break;
    		case 4:
    			observeItem();
    			break;
    		case 5:
    			setMyStrategy();
    			break;
			default:
				System.err.println("This should never execute");
    		}
    	}
    }
    
    private void printMenu() {
    	System.out.println("What you like to do?");
    	System.out.println("1) Print pending auctions");
    	System.out.println("2) Place item to auction");
    	System.out.println("3) Bid the item");
    	System.out.println("4) Observe the item (will notify you about changes in bid)");
    	System.out.println("5) Set AI for observed items");
    	System.out.println("0) Disconnect from auction");
    }
    
    private int getMenuPosition(int leftRange, int rightRange) {
    	int position = -1;
    	while(!(position >= leftRange && position <= rightRange)) {
    		System.out.println(">Whats your choice?");
    		try {
    			position = Integer.parseInt(new Scanner(System.in).nextLine());
    		}
    		catch (Exception e) {
    			System.out.println("Error reading value from console");
    		}
    	}
    	return position;
    }
    
    private void printPendingAuctions() {
    	try {
	    	Item items[] = auctionServer.getItems();
	    	for (Item item : items) {
	    		System.out.println("Item Name: " + item.getName());
	    		System.out.println("Owner: " + item.getOwnerName());
	    		System.out.println("Current bid: " + item.getCurrentBid());
	    		System.out.println("Bid by: " + item.getCurrentBiddersName());
	    		System.out.println("Biddable for: " + Long.toString(item.getRemainingTime()) + " seconds");
	    		System.out.println("-----------------");
	    	}
    	}
    	catch (Exception e) {
    		System.err.println("Something went wrong...");
    		e.printStackTrace();
    	}
    }
    
    private void placeItemOnAuction() {
    	String itemName, itemDescription;
    	double initialBid = 5.0;
    	int auctionTime = 300;	// seconds
    	System.out.println(">What are You placing to auction?");
    	itemName = new Scanner(System.in).nextLine();
    	System.out.println(">Describe item in one line...");
    	itemDescription = new Scanner(System.in).nextLine();
    	System.out.println(">What's inital bid?(format 12.34)");
    	try {
    		initialBid = Double.parseDouble(new Scanner(System.in).nextLine());
    	}
    	catch (Exception e) {
    		System.err.println("Sorry, unable to use it. Setting default bid to 5.0");
    	}
    	System.out.println(">How long auction should be on going?(place value in seconds)");
    	try {
    		auctionTime = Integer.parseInt(new Scanner(System.in).nextLine());
    		if (auctionTime < 1) {
    			auctionTime = 300;
    			System.out.println("Overriding duration to: " + Integer.toString(auctionTime));
    		}
    	}
    	catch (Exception e) {
    		System.err.println("Sorry, unable to use it. Setting duration to 300s");
    	}
    	
    	//perform on server
    	try {
    		if (name.length() < 2) {
    			name = "John";
    			System.err.println("Oveeriding your name to John");
    		}
    		
    		auctionServer.placeItemForBid(name, itemName, itemDescription, initialBid, auctionTime);
    		System.out.printf("Item %s placed for auction successfuly\n", itemName);
    	}
    	catch (RemoteException e) {
    		System.out.println("Unable to place a bid for a following reason: " + e.getMessage());
    	}
    }
    
    private void bidItem() {
    	String itemName;
    	double bid = 5.0;
    	
    	System.out.println(">What item are you trying to bid?");
    	itemName = new Scanner(System.in).nextLine();
    	
    	System.out.println(">What's your bid?");
    	try {
    		bid = Double.parseDouble(new Scanner(System.in).nextLine());
    	}
    	catch (Exception e) {
    		bid = 5.0;
    		System.err.println("Sorry, invalid value. Setting default bid to 5.0");
    	}
    	
    	//perform on server
    	try {
    		if (name.length() < 2) {
    			name = "John";
    			System.err.println("Oveeriding your name to John");
    		}
    		
    		auctionServer.bidOnItem(name, itemName, bid);
    		System.out.printf("Item %s bid for %f with success!\n", itemName, bid);
    	}
    	catch (RemoteException e) {
    		System.out.println("Failed to place bid for a following reason: " + e.getMessage());
    	}
    }
    
    private void observeItem() {
    	String itemName;
    	System.out.println(">What item are you want to observe?");
    	itemName = new Scanner(System.in).nextLine();
    	
    	//perform on server
    	try {
    		
    		auctionServer.registerListener(this, itemName);
    		System.out.printf("Successfuly registered for observing %s!\n", itemName);
    	}
    	catch (RemoteException e) {
    		System.out.println("Failed to start observing item. Reason: " + e.getMessage());
    	}
    }
    
    private void setMyStrategy() {
    	double maximumBid = 100.0;
    	System.out.println("Choose your new strategy: ");
    	System.out.println("0) Manual");
    	System.out.println("1) Automatically overbid by 1 unit until specified bid value");
    	System.out.println("2) Overbid by 100% item bid value in last second");
    	
    	switch(getMenuPosition(0,2)) {
    	case 0:
    		setStrategy(null);
    		break;
    	case 1:
    		try {
    			System.out.println(">Type maximum price for automatic bid.");
    			maximumBid = Double.parseDouble(new Scanner(System.in).nextLine());
    			setStrategy(new LimitedIncreaseStrategy(maximumBid));
    		}
    		catch (Exception e) {
    			System.out.println("Failed to set auto-bid-increasing strategy!");
    		}
    		break;
    	case 2:
    		setStrategy(new OverbidStrategy());
    		break;
		default:
			System.out.println("Should never be executed...");
    	}
    }
    
	public IAuthAuctionServer getAuctionServer() {
		return auctionServer;
	}

	@Override
	public String getPass() throws RemoteException {
		// TODO Auto-generated method stub
		return pass;
	}

	@Override
	public String getLogin() throws RemoteException {
		// TODO Auto-generated method stub
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	
}
