package client;

import java.rmi.RemoteException;
import java.util.Scanner;

import server.IAuthAuctionListener;

public class AuthClient extends Client implements IAuthAuctionListener{

	private String login;
	private String pass;
	
	protected AuthClient() throws RemoteException {
		super();
	}
	
	@Override
	public void connect(String arg) throws Exception {
		// TODO Auto-generated method stub
		super.connect(arg);

		System.out.println("Authenticate: ");
		System.out.println("Login: ");
		this.login = new Scanner(System.in).nextLine();
		System.out.println("Password: ");
		this.pass = new Scanner(System.in).nextLine();
		
		auctionServer.login(this);
	}
	
	@Override
	public String getLogin() throws RemoteException {
		// TODO Auto-generated method stub
		return login;
	}
	
	@Override
	public String getPass() throws RemoteException {
		// TODO Auto-generated method stub
		return pass;
	}

}
