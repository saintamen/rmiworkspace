package client;

import server.*;
import java.rmi.RemoteException;

public class LimitedIncreaseStrategy implements Strategy {
	protected double maximumBid;
	
	public double getMaximumBid() {
		return maximumBid;
	}
	
	public void setMaximumBid(double maximumBid) {
		this.maximumBid = maximumBid;
	}
	
	public LimitedIncreaseStrategy(double maximumBid) {
		this.maximumBid = maximumBid;
	}
	
	public void updateWithStrategy(Item item, Client client, IAuctionServer auctionServer) {
		//if able, overbid
		if (item.getCurrentBid() + 1.0 <= maximumBid &&
				!client.getName().equals(item.getCurrentBiddersName())) {
			
			double overbid = item.getCurrentBid();
			RemoteException e;
			
			do {
				e = null;
				overbid += 1.0;
				try {
					auctionServer.bidOnItem(client.getName(), item.getName(), overbid);
				}
				catch (RemoteException re) {
					e = re;
				}
				
				//stop bidding...
				if (overbid + 1.0 > maximumBid) {
					break;
				}
			} while (e != null);
		}
	}
	
	//say who are you
	@Override
	public String toString() {
		return "LimitedIncreaseStrategy";
	}
}